---
layout: category
title: Marcação
---
**Markdown**

"Markdown é uma linguagem simples de marcação originalmente criada por John Gruber e Aaron Swartz. Markdown converte seu texto em HTML válido. Markdown é frequentemente usado para formatar arquivos README, para escrever mensagens em fóruns de discussão online e para criar rich text usando um editor de texto simples." [Wikipédia](https://pt.wikipedia.org/wiki/Markdown)

Entenda e aprenda com o [Guia Markdown](https://www.markdownguide.org).
