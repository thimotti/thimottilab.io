---
layout: category
title: Cibersegurança
---

A Cibersegurança ou Segurança da Informação (SI) está diretamente relacionada com proteção de um conjunto de informações, no sentido de preservar o valor que possuem para um indivíduo ou uma organização, e a proteção de sistemas de computador contra roubo ou danos ao hardware, software ou dados eletrônicos, bem como a interrupção ou desorientação dos serviços que fornecem.

E esta é a minha tentantiva de documentar o meu estudo sobre o tema.

Abaixo algumas postagens relacionadas: 
