---
layout: post
title: "CiSSP 05: Criptografia"
categories:
  - Cibersegurança
tags:
  - CISSP
  - CBK
  - Revisão
last_modified_at: 2021-02-15T21:10:00-03:00
---

## Domínio de Criptografia      

Certified Information Systems Security Professional (CISSP)®

Common Body of Knowledge (CBK)®
