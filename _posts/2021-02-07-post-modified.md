---
layout: post
title: "Post: Data de Modificação"
categories:
  - Post Formats
tags:
  - Post Formats
  - readability
  - standard
last_modified_at: 2021-02-17T16:01:27-03:00
---

Esta postagem foi atualizada e deve mostrar uma data de modificação se `last_modified_at` for usada no layout.

Plugins como [**jekyll-sitemap**](https://github.com/jekyll/jekyll-feed) usam este campo para adicionar uma tag `<lastmod>` no arquivo `sitemap.xml`.
