---
layout: post
title: "CiSSP 09: Planejamento de Continuidade de Negócios e Recuperação de Desastres"
categories:
  - Cibersegurança
tags:
  - CISSP
  - CBK
  - Revisão
last_modified_at: 2021-02-19T21:10:00-03:00
---

## Domínio de Planejamento de Continuidade de Negócios e Recuperação de Desastres

Certified Information Systems Security Professional (CISSP)®

Common Body of Knowledge (CBK)®
