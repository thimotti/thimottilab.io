---
layout: post
title: "Post: Twitter Embed"
categories:
  - Media
tags:
  - conteúdo
  - media
  - twitter
last_modified_at: 2021-02-17T12:57:42-03:00
---

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Open Source por David Thimotti <a href="https://t.co/2ZSzK4aGBU">https://t.co/2ZSzK4aGBU</a></p>&mdash; David Thimotti (@thimotti) <a href="https://twitter.com/thimotti/status/1153668930885623808?ref_src=twsrc%5Etfw">July 23, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Esta postagem testa as incorporações do Twitter.
