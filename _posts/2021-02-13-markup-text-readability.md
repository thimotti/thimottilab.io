---
layout: post
title: "Marcação: Teste de Legibilidade de Texto"
excerpt: "Texto de amostra para testar a legibilidade de uma página com muito texto"
categories:
  - Marcação
tags:
  - Post
  - Legibilidade
  - Teste
last_modified_at: 2021-02-17T10:45:16-03:00
---

Escrever bem contribui com o marketing de conteúdo. Mas, será que os textos são tão claros quanto as ideias?

O texto deve ser claro, coerente e coeso. Mas, na prática: como fazer? Como tornar a redação o mais legível possível?

Texto legível é aquele fácil de ler e compreender. **David Ogilvy** já dizia: *“Comunicação não é o que você diz, mas o que o público entende”*.

Três características são fundamentais para que o texto seja legível ou fácil de entender:

## 1. Clareza

Uma das coisas que mais proporcionam clareza ao texto é usar frases curtas. A inimiga número um da clareza é a frase longa.

Na internet, é importante evitar frases maiores do que uma linha e meia (em Arial ou Times New Roman 12). Parágrafos não devem ultrapassar as quatro linhas. Estourando, cinco. O mesmo vale para as palavras: prefira as mais curtas.

## 2. Coerência

O texto tem que fazer sentido. E não pode ser contraditório.

Nada mais estranho do que frases do tipo: “Venha depressa garantir sua tranquilidade!” Puxa, tenho mesmo que ir depressa se tudo o que eu quero é tranquilidade?

## 3. Coesão

Um texto coeso é aquele em que os parágrafos são ligados harmonicamente. Como?  Retomando palavras, expressões ou frases já ditas no parágrafo anterior ou ainda, antecipando o que vai ser dito no próximo.
