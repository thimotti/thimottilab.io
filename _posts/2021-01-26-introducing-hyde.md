---
layout: post
title: Apresentando Hyde
---

Hyde é um tema do [Jekyll](http://jekyllrb.com) de duas colunas que combina uma barra lateral proeminente com conteúdo descomplicado. É baseado no [Poole](http://getpoole.com).

### Construído em Poole

Poole é o mordomo de Jekyll, servindo como uma base sólida e eficaz para os temas de Jekyll. O Poole e cada tema construído sobre ele (como Hyde aqui) inclui o seguinte::

* Configuração completa do Jekyll incluída (layouts, config, [404]({{ "/404" | relative_url }}), [RSS feed]({{ "/feed.xml" | relative_url }}), posts e [a página Sobre]({{ "/about" | relative_url }}))
* Design e desenvolvimento otimizado para celular
* Texto facilmente escalável e dimensionamento de componentes com unidades `rem` no CSS
* Suporte para uma ampla gama de elementos HTML
* Postagens relacionadas (baseadas no tempo) abaixo de cada postagem
* Destaque de sintaxe, cortesia de Pygments (o destacador de snippet de código baseado em Python)

### Recursos Hyde

Além dos recursos do Poole, Hyde adiciona o seguinte:

* A barra lateral inclui suporte para módulos textuais e uma navegação gerada dinamicamente com suporte de link ativo
* Duas orientações para conteúdo e barra lateral, padrão (barra lateral esquerda) e [reversa](https://github.com/poole/hyde#reverse-layout) (barra lateral direita), disponível via classes `<body>`
* [Oito esquemas de cores opcionais](https://github.com/poole/hyde#themes), disponível via classes `<body>`

[Leio o readme](https://github.com/poole/hyde#readme) para saber mais.

### Browser suportados

Hyde é, de preferência, um projeto com visão de futuro. Além das versões mais recentes do Chrome, Safari (móvel e desktop) e Firefox, ele é compatível, somente, com o Internet Explorer 9 e superior..

### Download

Hyde é desenvolvido e hospedado no GitHub. Vá para o <a href="https://github.com/poole/hyde">Repositório GitHub</a> para downloads, reporte de bugs e solicitações de features.

Obrigado!
