---
title: "Marcação: Destaque de sintaxe"
layout: post
categories:
  - Marcação
excerpt: "várias maneiras de realizar postagem com destaque de blocos de código com o Jekyll. Algumas opções incluem  padrão Markdown, GitHub Flavored Markdown, e tags do Jekyll `{% highlight %}`."
last_modified_at: 2021-02-16T18:45:16-03:00
tags:
  - code
  - syntax highlighting
---

Realce de sintaxe (ou coloração de sintaxe) é uma funcionalidade disponível em alguns editores de texto que apresenta texto — em especial código fonte — numa formatação específica para cada categoria de termos. A mudança da formatação envolve alterações da fonte tipográfica, e principalmente, da coloração do texto. Essa funcionalidade facilita a escrita em linguagens estruturadas como linguagens de programação ou linguagens de marcação, já que as estruturas e os erros de sintaxe são facilmente distinguidos.

Para editores de texto que suportam mais de uma linguagem, o utilizador pode especificar a linguagem do texto, mas o editor também pode reconhecer automaticamente, baseado na extensão do arquivo ou analisando seu conteúdo.

Já que a maioria dos editores implementam a funcionalidade através e casamento de padrões ao invés de implementar um analisador sintático para cada linguagem possível, o que seria muito complexo, o realce nem sempre é completamente correto. Dependendo do mecanismo de casamento de padrões, o algoritmo de realce pode ser bastante lento para certos tipos de estruturas.[^1]

[^1]: <https://pt.wikipedia.org/wiki/Realce_de_sintaxe>

## Blocos de código GFM

Os [ blocos de código protegidos](https://help.github.com/articles/creating-and-highlighting-code-blocks/) do GitHub Flavored Markdown são suportados por padrão no Jekyll. Atualize o arquivo `_config.yml` para habilitá-los se estiver usando uma versão mais antiga.

```yaml
kramdown:
  input: GFM
```

Aqui está um exemplo de um snippet de código CSS escrito em GFM:

```css
#container {
  float: left;
  margin: 0 -240px 0 0;
  width: 100%;
}
```

Ainda outro snippet de código para fins de demonstração:

```ruby
module Jekyll
  class TagIndex < Page
    def initialize(site, base, dir, tag)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'
      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'tag_index.html')
      self.data['tag'] = tag
      tag_title_prefix = site.config['tag_title_prefix'] || 'Tagged: '
      tag_title_suffix = site.config['tag_title_suffix'] || '&#8211;'
      self.data['title'] = "#{tag_title_prefix}#{tag}"
      self.data['description'] = "An archive of posts tagged #{tag}."
    end
  end
end
```

## Jekyll Highlight Liquid Tag

Jekyll também tem suporte integrado para destaque de sintaxe de trechos de código usando Rouge ou Pygments, usando uma tag Liquid dedicada da seguinte maneira:

```liquid
{% raw %}{% highlight scss %}
.highlight {
  margin: 0;
  padding: 1em;
  font-family: $monospace;
  font-size: $type-size-7;
  line-height: 1.8;
}
{% endhighlight %}{% endraw %}
```

E a saída será semelhante a esta:

{% highlight scss %}
.highlight {
  margin: 0;
  padding: 1em;
  font-family: $monospace;
  font-size: $type-size-7;
  line-height: 1.8;
}
{% endhighlight %}

Aqui está um exemplo de um snippet de código usando a tag Liquid e`linenos` habilitado.

{% highlight html linenos %}
{% raw %}<nav class="pagination" role="navigation">
  {% if page.previous %}
    <a href="{{ site.url }}{{ page.previous.url }}" class="btn" title="{{ page.previous.title }}">Previous article</a>
  {% endif %}
  {% if page.next %}
    <a href="{{ site.url }}{{ page.next.url }}" class="btn" title="{{ page.next.title }}">Next article</a>
  {% endif %}
</nav><!-- /.pagination -->{% endraw %}
{% endhighlight %}

## Blocos de código em listas

O recuo é importante. Certifique-se de que o recuo do bloco de código esteja alinhado com o primeiro caractere sem espaço após o marcador de item da lista (por exemplo, `1.`). Normalmente, isso significará recuar 3 espaços em vez de 4.

1. Execute a etapa 1.
2. Agora faça isso:

   ```ruby
   def print_hi(name)
     puts "Hi, #{name}"
   end
   print_hi('Tom')
   #=> prints 'Hi, Tom' to STDOUT.
   ```

3. Agora você pode fazer isso.

## GitHub Gist Embed

As incorporações do GitHub Gist também podem ser usadas:

```html
<script src="https://gist.github.com/mmistakes/77c68fbb07731a456805a7b473f47841.js"></script>
```

Que resulta na saída:

<script src="https://gist.github.com/mmistakes/77c68fbb07731a456805a7b473f47841.js"></script>
