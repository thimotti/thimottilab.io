---
layout: post
title: "CiSSP 04: Segurança de Operações"
categories:
  - Cibersegurança
tags:
  - CISSP
  - CBK
  - Revisão
last_modified_at: 2021-02-14T21:10:00-03:00
---

## Domínio de Segurança de Operações        

Certified Information Systems Security Professional (CISSP)®

Common Body of Knowledge (CBK)®
