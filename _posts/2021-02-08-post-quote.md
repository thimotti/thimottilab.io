---
layout: post
title: "Post: Citação"
categories:
  - Post Formats
tags:
  - Post Formats
  - quote
---

> Só uma coisa é impossível para Deus: encontrar algum sentido em qualquer lei de direitos autorais no planeta.
>
> <cite><a href="http://www.brainyquote.com/quotes/quotes/m/marktwain163473.html">Mark Twain</a></cite>
