---
layout: post
title: "CiSSP 02: Design e Arquitetura de Segurança"
categories:
  - Cibersegurança
tags:
  - CISSP
  - CBK
  - Revisão
last_modified_at: 2021-02-16T21:10:00-03:00
---

## Domínio de Design e Arquitetura de Segurança        

Certified Information Systems Security Professional (CISSP)®

Common Body of Knowledge (CBK)®
