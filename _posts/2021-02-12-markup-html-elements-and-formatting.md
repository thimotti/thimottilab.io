---
layout: post
title: "Marcação: Elementos e formatação HTML"
sub_title: "Os elementos comuns"
categories:
  - Marcação
elements:
  - content
  - css
  - formatting
  - html
  - markup
last_modified_at: 2021-02-16T18:50:01-03:00
---

Uma variedade de elementos HTML comuns para demonstrar a folha de estilo do site e verificar se eles foram estilizados de forma adequada.

# H1

## H2

### H3

#### H4

##### H5

###### H6

## Citações em bloco

Bloco de linha única:

> Permaneça faminto. Permaneça tolo.

Bloco multilinha com uma referência de citação::

> People think focus means saying yes to the thing you've got to focus on. But that's not what it means at all. It means saying no to the hundred other good ideas that there are. You have to pick carefully. I'm actually as proud of the things we haven't done as the things I have done. Innovation is saying no to 1,000 things.

<cite>Steve Jobs</cite> --- Apple Worldwide Developers' Conference, 1997
{: .small}

## Tabelas

| Empregado        | Salário |                                                             |
| --------         | ------ | ------------------------------------------------------------ |
| [John Doe](#)    | R$1     | Porque isso é tudo que Steve Jobs precisava para um salário.|
| [Jane Doe](#)    | R$100K  | Por todos os blogs que ela faz.                             |
| [Fred Bloggs](#) | R$100M  | Imagens valem mais que mil palavras, certo? Então × 1.000.  |
| [Jane Bloggs](#) | R$100B  | Com cabelo assim ?! Disse o suficiente.                     |

| Header1 | Header2 | Header3 |
|:--------|:-------:|--------:|
| cell1   | cell2   | cell3   |
| cell4   | cell5   | cell6   |
|-----------------------------|
| cell1   | cell2   | cell3   |
| cell4   | cell5   | cell6   |
|=============================|
| Foot1   | Foot2   | Foot3   |

## Listas de Definição

Título da lista de definição
: Divisão da lista de definições.

Startup
: Uma startup é uma empresa ou organização temporária projetada para buscar um modelo de negócios escalonável e repetível..

#dowork
: Cunhado por Rob Dyrdek e seu guarda-costas pessoal Christopher “Big Black” Boykins, “Do Work” funciona como um auto-motivador, para motivar seus amigos..

Do It Live
: Vou deixar Bill O'Reilly [explicar](https://www.youtube.com/watch?v=O_HyZ5aW76c "We'll Do It Live") isso.

## Listas não ordenadas (aninhadas)

  * Item 1 da lista 1
      * Item 1 da lista 2
          * Item 1 da lista 3
          * Item 2 da lista 3
          * Item 3 da lista 3
          * Item 4 da lista 3
      * Item 2 da lista 2
      * Item 3 da lista 2
      * Item 4 da lista 2
  * Item 2 da lista 1
  * Item 3 da lista 1
  * Item 4 da lista 1

## Lista ordenada (aninhada)

  1. Item da lista 1
      1. Item da lista 2
          1. Item da lista 3
          2. Item da lista 3
          3. Item da lista 3
          4. Item da lista 3
      2. Item da lista 2
      3. Item da lista 2
      4. Item da lista 2
  2. Item da lista 1
  3. Item da lista 1
  4. Item da lista 1

## Elemento de endereço

<address>
  David Thimotti<br /> Belo Horizonte-MG | 30.770-520<br /> Brasil
</address>

## Elemento âncora (também conhecido como Link)

Este é um exemplo de [link](http://apple.com "Apple").

## Elemento de abreviatura

A abreviatura CSS significa "Cascading Style Sheets".

*[CSS]: Cascading Style Sheets

## Elemento de Citação

"Código é poesia." ---<cite>Automattic</cite>

## Elemento de código

Ainda vou aprender que `word-wrap: break-word;` são meus melhores amigos.

## Elemento de Strike

Este elemento permite <strike>riscar um texto</strike>.

## Elemento de Ênfase

O elemento de ênfase deve colocar o texto em _itálico_.

## Elemento de Inserção

O elemento Inserção mostra <ins>o texto inserido</ins>.

## Elemento de Keyboard

Este elemento quase desconhecido emula <kbd>keyboard text</kbd>, que geralmente é denominado como o elemento `<code>`.

## Elemento pré-formatado

Este elemento estiliza grandes blocos de código.

<pre>
.post-title {
	margin: 0 0 5px;
	font-weight: bold;
	font-size: 38px;
	line-height: 1.2;
        Aqui está uma linha de um texto muito, muito, muito, muito longo, apenas para ver como o elemento lida com isso e descobrir como ele transborda todo o texto;
}
</pre>

## Elemento de citação (Quote)

<q>Desenvolvedores, desenvolvedores, desenvolvedores ...&#8230;</q> &#8211;Steve Ballmer

## Elemento Strong

Este elemento mostra **texto em negrito**.

## Elemento subscrito

Fazendo nossa ciência estilizar com H<sub>2</sub>O, o que deve empurrar o “2” para baixo.

## Elemento sobrescrito

Ainda mantendo a ciência e Einstein, E = MC<sup>2</sup>, o que deve levantar o 2.

## Elemento variável

Isso permite que você denote <var>variáveis</var>.
