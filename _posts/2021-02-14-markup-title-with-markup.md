---
layout: post
title: "Marcação: Título *com* **Markdown**"
categories:
  - Marcação
tags:
  - css
  - html
last_modified_at: 2021-02-16T18:45:16-03:00
---

O uso de Markdown no título não deve ter efeito adverso no layout ou funcionalidade.

**`page.title` exemplo:**

```yaml
title: "Marcação: Título *com* **Markdown**"
```
