---
layout: post
title: "Post: Padrão"
excerpt_separator: "<!--more-->"
categories:
  - Post Formats
tags:
  - Post Formats
  - readability
  - standard
---

Todas as crianças, exceto uma, crescem. Eles logo sabem que vão crescer, e Wendy sabia disso. Um dia, quando ela tinha dois anos, ela estava brincando em um jardim, e ela colheu outra flor e correu com ela para sua mãe. Suponho que ela deve ter parecido bastante encantadora, pois a Sra. Darling levou a mão ao coração e gritou: "Oh, por que você não pode permanecer assim para sempre!" Isso foi tudo o que se passou entre eles sobre o assunto, mas daí em diante Wendy sabia que ela deveria crescer. Você sempre sabe depois de dois anos. Dois é o começo do fim.

A Sra. Darling ouviu falar de Peter pela primeira vez quando ela estava organizando a mente de seus filhos. É o costume noturno de toda boa mãe, depois que seus filhos estão dormindo, vasculhar suas mentes e acertar as coisas para a manhã seguinte, colocando de volta em seus devidos lugares os muitos artigos que vagaram durante o dia.

<!--more-->

Esta postagem tem a inserção manual do excerpt `<!--more-->` após o segundo parágrafos. YAML Front Matter aplicado:

```yaml
excerpt_separator: "<!--more-->"
```

Se você pudesse ficar acordado (mas é claro que não pode), veria sua própria mãe fazendo isso e acharia muito interessante observá-la. É como arrumar gavetas. Você a veria de joelhos, suponho, demorando-se humoristicamente sobre alguns de seus conteúdos, perguntando-se onde diabos você pegou essa coisa, fazendo descobertas doces e não tão doces, pressionando isto em sua bochecha como se fosse tão bom quanto um gatinho, e rapidamente guardando-o fora de vista. Quando você acorda de manhã, as travessuras e paixões malignas com que você foi para a cama foram dobradas e colocadas no fundo da sua mente e no topo, lindamente arejadas, espalham seus pensamentos mais bonitos, prontos para você colocar.

Não sei se você já viu um mapa da mente de uma pessoa. Os médicos às vezes desenham mapas de outras partes de você, e seu próprio mapa pode se tornar intensamente interessante, mas pegue-os tentando desenhar um mapa da mente de uma criança, que não só é confuso, mas continua girando o tempo todo. Existem linhas em ziguezague nele, assim como sua temperatura em um cartão, e essas são provavelmente as estradas da ilha, pois Neverland é sempre mais ou menos uma ilha, com salpicos surpreendentes de cor aqui e ali, e recifes de coral e rakish- procurando embarcações à distância, selvagens e tocas solitárias, gnomos que são em sua maioria alfaiates e cavernas através das quais corre um rio, e príncipes com seis irmãos mais velhos e uma cabana prestes a se deteriorar, e uma velhinha muito pequena com um nariz curvo. Seria um mapa fácil se isso fosse tudo, mas também há primeiro dia na escola,

É claro que Neverlands varia bastante. O John's, por exemplo, tinha uma lagoa com flamingos voando sobre ela, na qual John estava atirando, enquanto Michael, que era muito pequeno, tinha um flamingo com lagoas voando sobre ele. John morava em um barco virado de cabeça para baixo na areia, Michael em uma cabana, Wendy em uma casa de folhas cuidadosamente costuradas. John não tinha amigos, Michael tinha amigos à noite, Wendy tinha um lobo de estimação abandonado por seus pais, mas no geral os Neverlands têm uma semelhança de família, e se eles ficassem parados, você poderia dizer deles que eles têm um ao outro nariz e assim por diante. Nessas praias mágicas, as crianças brincando estão para sempre encalhando seus coráculos [barco simples]. Nós também estivemos lá; ainda podemos ouvir o som das ondas, embora não devamos pousar mais.

De todas as ilhas deleitáveis, Neverland é a mais confortável e compacta, não grande e esparramada, você sabe, com distâncias tediosas entre uma aventura e outra, mas bem lotada. Quando você brinca com isso de dia com as cadeiras e a toalha de mesa, não é nem um pouco alarmante, mas nos dois minutos antes de você ir dormir torna-se muito real. É por isso que existem luzes noturnas.

Ocasionalmente, em suas viagens pela mente de seus filhos, a Sra. Darling encontrava coisas que não conseguia entender, e entre elas a mais desconcertante era a palavra Peter. Ela não conhecia Peter, mas ele estava aqui e ali nas mentes de John e Michael, enquanto Wendy começava a ser rabiscada com ele. O nome se destacava em letras mais fortes do que qualquer uma das outras palavras, e enquanto a Sra. Darling olhava, ela sentiu que tinha uma aparência estranhamente arrogante.
