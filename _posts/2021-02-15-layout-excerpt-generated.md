---
layout: post
title: "Layout: Excerpt (Tag Separator)"
excerpt_separator: "<!--Mais-->"
categories:
  - Layout
tags:
  - conteúdo
  - excerpt
  - layout
last_modified_at: 2021-02-16T18:45:16-03:00
---

O conteúdo desta postagem, e de todas anteriores ao dia 16-02-2021, foram criados como uma forma de estudo/documentação do Blog.

As páginas de índice de arquivo devem exibir um [trecho gerado automaticamente](https://jekyllrb.com/docs/posts/#post-excerpts)  de todo o conteúdo anterior `excerpt_separator`, conforme definido no YAML Front Matter ou globalmente em `_config.yml`.

Testes da formatação do trecho gerado automaticamente, para garantir que não crie problemas no layout.

<!--Mais-->

Neste pequeno trecho, vamos explorar uma opção para mostrar um resumo de sua postagem ou artigo no blog, se você estiver usando o incrível gerador de site estático chamado Jekyll.
Estou usando o gerador de site estático Jekyll para compilar meu site em um monte de arquivos estáticos (o principal benefício é um site geralmente mais seguro e potencialmente mais rápido devido à falta de viagens de ida e volta do servidor para um banco de dados).

Eu encontrei várias soluções para exibir um resumo ou trecho do post na página principal do blog (se você estiver usando o tema Jekyll básico que vem de fábrica, provavelmente será o seu index.html).

De qualquer forma, a solução que encontrei visa permitir-me controlar que parte da postagem desejo exibir como um trecho. Se eu não escolher uma parte da postagem, tudo bem, ela apenas exibe um trecho padrão.

Se você não sabia, Jekyll usa Liquid para seus modelos.

[fonte: ](http://frontendcollisionblog.com/jekyll/snippet/2015/03/23/how-to-show-a-summary-of-your-post-with-jekyll.html)
