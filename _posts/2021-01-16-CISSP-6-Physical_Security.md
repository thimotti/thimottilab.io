---
layout: post
title: "CiSSP 06: Segurança Física"
categories:
  - Cibersegurança
tags:
  - CISSP
  - CBK
  - Revisão
last_modified_at: 2021-02-16T21:10:00-03:00
---

## Domínio de Segurança Física        

Certified Information Systems Security Professional (CISSP)®

Common Body of Knowledge (CBK)®
