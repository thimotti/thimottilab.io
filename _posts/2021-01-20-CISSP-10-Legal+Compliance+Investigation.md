---
layout: post
title: "CISSP 10: Jurídico, Regulamentações, Conformidade e Investigação"
categories:
  - Cibersegurança
tags:
  - CISSP
  - CBK
  - Revisão
last_modified_at: 2021-02-20T21:10:00-03:00
---

## Domínio Jurídico, Regulamentações, Conformidade e Investigação        

Certified Information Systems Security Professional (CISSP)®

Common Body of Knowledge (CBK)®
