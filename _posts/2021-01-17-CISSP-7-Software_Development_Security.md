---
layout: post
title: "CiSSP 07: Segurança de Desenvolvimento de Software"
categories:
  - Cibersegurança
tags:
  - CISSP
  - CBK
  - Revisão
last_modified_at: 2021-02-17T21:10:00-03:00
---

## Domínio de Segurança de Desenvolvimento de Software        

Certified Information Systems Security Professional (CISSP)®

Common Body of Knowledge (CBK)®
