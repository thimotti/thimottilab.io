---
layout: post
title: 'Projetos WEB'
excerpt_separator: "<!--Mais-->"
categories:
  - Documentação
tags:
  - Projetos
last_modified_at: 2021-02-18T11:20:10-03:00
---
Agora sou **Web Developer** !!!

<!--Mais-->

Brincadeiras a parte, eu não mudei de profissão, eu sou Analista de Redes e Segurança (Network Security Analyst), mas aprender nunca é mais demais e tenho adquirido muito conhecimento nesta nova jornada. Abaixo meus trabalhos WEB.

* [thimotti.gitlab.io](https://thimotti.gitlab.io)
* [thimotti.github.io](https://thimotti.github.io)
* [thimotti.eu.org](https://thimotti.eu.org)
* [thimotti.tk](https://thimotti.tk)
* [thimotti.ml](https://thimotti.ml)
* [thimotti.cf](https://thimotti.cf)
* [thimotti.ga](https://thimotti.ga)
* [thimotti.gq](https://thimotti.gq)
