---
layout: post
title: O que é Jekyll?
---

[Jekyll](http://jekyllrb.com) é um gerador de sites estáticos, uma ferramenta de código aberto para criar sites simples, mas poderosos, de todas as formas e tamanhos. Arquivo [readme](https://github.com/mojombo/jekyll/blob/master/README.markdown) do projeto:

  > Jekyll is a simple, blog aware, static site generator. It takes a template directory [...] and spits out a complete, static website suitable for serving with Apache or your favorite web server. This is also the engine behind GitHub Pages, which you can use to host your project’s page or blog right here from GitHub.

É uma ferramenta extremamente útil e incentivo você a usar.

Saiba mais [visitando o projeto no GitHub](https://github.com/mojombo/jekyll).
