---
layout: post
title: "Post: Video (YouTube)"
categories:
  - Media
tags:
  - Post Formats
last_modified_at: 2021-02-17T15:33:37-03:00
---

<div class="video-container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/yWoLbeDC100" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

Esta é uma postagem de teste de vídeos do YouTube.

Basta envolver as incorporações com um elemento `<div>` e as classes apropriadas:

```html
<!-- 16:9 aspect ratio -->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="..."></iframe>
</div>

<!-- 4:3 aspect ratio -->
<div class="embed-responsive embed-responsive-4by3">
  <iframe class="embed-responsive-item" src="..."></iframe>
</div>

<!-- responsive iframe. The framesize reduces proportionately when viewing in mobile -->
<div class="video-container">
  <iframe class="embed-responsive-item" src="..."></iframe>
</div>
```
