I"�*<p>Realce de sintaxe (ou coloração de sintaxe) é uma funcionalidade disponível em alguns editores de texto que apresenta texto — em especial código fonte — numa formatação específica para cada categoria de termos. A mudança da formatação envolve alterações da fonte tipográfica, e principalmente, da coloração do texto. Essa funcionalidade facilita a escrita em linguagens estruturadas como linguagens de programação ou linguagens de marcação, já que as estruturas e os erros de sintaxe são facilmente distinguidos.</p>

<p>Para editores de texto que suportam mais de uma linguagem, o utilizador pode especificar a linguagem do texto, mas o editor também pode reconhecer automaticamente, baseado na extensão do arquivo ou analisando seu conteúdo.</p>

<p>Já que a maioria dos editores implementam a funcionalidade através e casamento de padrões ao invés de implementar um analisador sintático para cada linguagem possível, o que seria muito complexo, o realce nem sempre é completamente correto. Dependendo do mecanismo de casamento de padrões, o algoritmo de realce pode ser bastante lento para certos tipos de estruturas.<sup id="fnref:1" role="doc-noteref"><a href="#fn:1" class="footnote">1</a></sup></p>

<h2 id="blocos-de-código-gfm">Blocos de código GFM</h2>

<p>Os <a href="https://help.github.com/articles/creating-and-highlighting-code-blocks/"> blocos de código protegidos</a> do GitHub Flavored Markdown são suportados por padrão no Jekyll. Atualize o arquivo <code class="language-plaintext highlighter-rouge">_config.yml</code> para habilitá-los se estiver usando uma versão mais antiga.</p>

<div class="language-yaml highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="na">kramdown</span><span class="pi">:</span>
  <span class="na">input</span><span class="pi">:</span> <span class="s">GFM</span>
</code></pre></div></div>

<p>Aqui está um exemplo de um snippet de código CSS escrito em GFM:</p>

<div class="language-css highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="nf">#container</span> <span class="p">{</span>
  <span class="nl">float</span><span class="p">:</span> <span class="nb">left</span><span class="p">;</span>
  <span class="nl">margin</span><span class="p">:</span> <span class="m">0</span> <span class="m">-240px</span> <span class="m">0</span> <span class="m">0</span><span class="p">;</span>
  <span class="nl">width</span><span class="p">:</span> <span class="m">100%</span><span class="p">;</span>
<span class="p">}</span>
</code></pre></div></div>

<p>Ainda outro snippet de código para fins de demonstração:</p>

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">module</span> <span class="nn">Jekyll</span>
  <span class="k">class</span> <span class="nc">TagIndex</span> <span class="o">&lt;</span> <span class="no">Page</span>
    <span class="k">def</span> <span class="nf">initialize</span><span class="p">(</span><span class="n">site</span><span class="p">,</span> <span class="n">base</span><span class="p">,</span> <span class="n">dir</span><span class="p">,</span> <span class="n">tag</span><span class="p">)</span>
      <span class="vi">@site</span> <span class="o">=</span> <span class="n">site</span>
      <span class="vi">@base</span> <span class="o">=</span> <span class="n">base</span>
      <span class="vi">@dir</span> <span class="o">=</span> <span class="n">dir</span>
      <span class="vi">@name</span> <span class="o">=</span> <span class="s1">'index.html'</span>
      <span class="nb">self</span><span class="p">.</span><span class="nf">process</span><span class="p">(</span><span class="vi">@name</span><span class="p">)</span>
      <span class="nb">self</span><span class="p">.</span><span class="nf">read_yaml</span><span class="p">(</span><span class="no">File</span><span class="p">.</span><span class="nf">join</span><span class="p">(</span><span class="n">base</span><span class="p">,</span> <span class="s1">'_layouts'</span><span class="p">),</span> <span class="s1">'tag_index.html'</span><span class="p">)</span>
      <span class="nb">self</span><span class="p">.</span><span class="nf">data</span><span class="p">[</span><span class="s1">'tag'</span><span class="p">]</span> <span class="o">=</span> <span class="n">tag</span>
      <span class="n">tag_title_prefix</span> <span class="o">=</span> <span class="n">site</span><span class="p">.</span><span class="nf">config</span><span class="p">[</span><span class="s1">'tag_title_prefix'</span><span class="p">]</span> <span class="o">||</span> <span class="s1">'Tagged: '</span>
      <span class="n">tag_title_suffix</span> <span class="o">=</span> <span class="n">site</span><span class="p">.</span><span class="nf">config</span><span class="p">[</span><span class="s1">'tag_title_suffix'</span><span class="p">]</span> <span class="o">||</span> <span class="s1">'&amp;#8211;'</span>
      <span class="nb">self</span><span class="p">.</span><span class="nf">data</span><span class="p">[</span><span class="s1">'title'</span><span class="p">]</span> <span class="o">=</span> <span class="s2">"</span><span class="si">#{</span><span class="n">tag_title_prefix</span><span class="si">}#{</span><span class="n">tag</span><span class="si">}</span><span class="s2">"</span>
      <span class="nb">self</span><span class="p">.</span><span class="nf">data</span><span class="p">[</span><span class="s1">'description'</span><span class="p">]</span> <span class="o">=</span> <span class="s2">"An archive of posts tagged </span><span class="si">#{</span><span class="n">tag</span><span class="si">}</span><span class="s2">."</span>
    <span class="k">end</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<h2 id="jekyll-highlight-liquid-tag">Jekyll Highlight Liquid Tag</h2>

<p>Jekyll também tem suporte integrado para destaque de sintaxe de trechos de código usando Rouge ou Pygments, usando uma tag Liquid dedicada da seguinte maneira:</p>

<div class="language-liquid highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="p">{%</span><span class="w"> </span><span class="nt">highlight</span><span class="w"> </span>scss<span class="w"> </span><span class="p">%}</span>
.highlight {
  margin: 0;
  padding: 1em;
  font-family: $monospace;
  font-size: $type-size-7;
  line-height: 1.8;
}
<span class="p">{%</span><span class="w"> </span><span class="nt">endhighlight</span><span class="w"> </span><span class="p">%}</span>
</code></pre></div></div>

<p>E a saída será semelhante a esta:</p>

<figure class="highlight"><pre><code class="language-scss" data-lang="scss"><span class="nc">.highlight</span> <span class="p">{</span>
  <span class="nl">margin</span><span class="p">:</span> <span class="m">0</span><span class="p">;</span>
  <span class="nl">padding</span><span class="p">:</span> <span class="m">1em</span><span class="p">;</span>
  <span class="nl">font-family</span><span class="p">:</span> <span class="nv">$monospace</span><span class="p">;</span>
  <span class="nl">font-size</span><span class="p">:</span> <span class="nv">$type-size-7</span><span class="p">;</span>
  <span class="nl">line-height</span><span class="p">:</span> <span class="m">1</span><span class="mi">.8</span><span class="p">;</span>
<span class="p">}</span></code></pre></figure>

<p>Aqui está um exemplo de um snippet de código usando a tag Liquid e<code class="language-plaintext highlighter-rouge">linenos</code> habilitado.</p>

<figure class="highlight"><pre><code class="language-html" data-lang="html"><table class="rouge-table"><tbody><tr><td class="gutter gl"><pre class="lineno">1
2
3
4
5
6
7
8
</pre></td><td class="code"><pre><span class="nt">&lt;nav</span> <span class="na">class=</span><span class="s">"pagination"</span> <span class="na">role=</span><span class="s">"navigation"</span><span class="nt">&gt;</span>
  {% if page.previous %}
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"{{ site.url }}{{ page.previous.url }}"</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">title=</span><span class="s">"{{ page.previous.title }}"</span><span class="nt">&gt;</span>Previous article<span class="nt">&lt;/a&gt;</span>
  {% endif %}
  {% if page.next %}
    <span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"{{ site.url }}{{ page.next.url }}"</span> <span class="na">class=</span><span class="s">"btn"</span> <span class="na">title=</span><span class="s">"{{ page.next.title }}"</span><span class="nt">&gt;</span>Next article<span class="nt">&lt;/a&gt;</span>
  {% endif %}
<span class="nt">&lt;/nav&gt;</span><span class="c">&lt;!-- /.pagination --&gt;</span>
</pre></td></tr></tbody></table></code></pre></figure>

<h2 id="blocos-de-código-em-listas">Blocos de código em listas</h2>

<p>O recuo é importante. Certifique-se de que o recuo do bloco de código esteja alinhado com o primeiro caractere sem espaço após o marcador de item da lista (por exemplo, <code class="language-plaintext highlighter-rouge">1.</code>). Normalmente, isso significará recuar 3 espaços em vez de 4.</p>

<ol>
  <li>Execute a etapa 1.</li>
  <li>
    <p>Agora faça isso:</p>

    <div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">def</span> <span class="nf">print_hi</span><span class="p">(</span><span class="nb">name</span><span class="p">)</span>
  <span class="nb">puts</span> <span class="s2">"Hi, </span><span class="si">#{</span><span class="nb">name</span><span class="si">}</span><span class="s2">"</span>
<span class="k">end</span>
<span class="n">print_hi</span><span class="p">(</span><span class="s1">'Tom'</span><span class="p">)</span>
<span class="c1">#=&gt; prints 'Hi, Tom' to STDOUT.</span>
</code></pre></div>    </div>
  </li>
  <li>Agora você pode fazer isso.</li>
</ol>

<h2 id="github-gist-embed">GitHub Gist Embed</h2>

<p>As incorporações do GitHub Gist também podem ser usadas:</p>

<div class="language-html highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="nt">&lt;script </span><span class="na">src=</span><span class="s">"https://gist.github.com/mmistakes/77c68fbb07731a456805a7b473f47841.js"</span><span class="nt">&gt;&lt;/script&gt;</span>
</code></pre></div></div>

<p>Que resulta na saída:</p>

<script src="https://gist.github.com/mmistakes/77c68fbb07731a456805a7b473f47841.js"></script>

<div class="footnotes" role="doc-endnotes">
  <ol>
    <li id="fn:1" role="doc-endnote">
      <p><a href="https://pt.wikipedia.org/wiki/Realce_de_sintaxe">https://pt.wikipedia.org/wiki/Realce_de_sintaxe</a> <a href="#fnref:1" class="reversefootnote" role="doc-backlink">&#8617;</a></p>
    </li>
  </ol>
</div>
:ET