I"�<h1 id="gitlab-pages">GitLab Pages</h1>

<p>Como é de conhecimento de todos, programação nunca foi a minha área (Até agora). Mas tudo muda, tudo mesmo, o tempo passa e novas necessidades e gostos são adicionados a nossa vida. O objetivo do blog é documentar meus estudos em geral, então, este pequeno tutorial demonstra como site foi construído.</p>

<!--Mais-->

<p><strong>GitLab Pages:</strong> A primeira parte foi entender o funcionamento das Páginas do GitLab, Documentação disponível em:  <a href="https://docs.gitlab.com/ee/user/project/pages/">GitLab Docs</a></p>

<p>Em resumo, o GitLab Pages, permite publicar <strong>sites estáticos</strong> diretamente de um repositório no GitLab.</p>

<p><strong>CI/CD</strong>: Outro tópico importante a ser entendido, o arquivo CI dá instruções ao <a href="https://docs.gitlab.com/runner/">runner</a>, quando o <a href="https://docs.gitlab.com/ee/ci/pipelines/index.html">pipeline</a> CI/CD é executado o site é criado.</p>

<p>Vou dedicar um post à CI/CD, pois é um assunto muito interessante e importante.</p>

<h1 id="jekkyl-e-gitlab">Jekkyl e Gitlab</h1>

<p><strong>Índice</strong></p>

<ul>
  <li><a href="#getting-started">Começando</a>
    <ul>
      <li><a href="#start-by-forking-this-repository">Comece bifurcando este repositório</a></li>
      <li><a href="#start-from-a-local-jekyll-project">Comece com um projeto Jekyll local</a></li>
    </ul>
  </li>
  <li><a href="#gitlab-ci">GitLab CI</a></li>
  <li><a href="#using-jekyll-locally">Usando Jekyll localmente</a></li>
  <li><a href="#gitlab-user-or-group-pages">Páginas de usuário ou grupo do GitLab</a></li>
  <li><a href="#did-you-fork-this-project">Você bifurcou este projeto?</a></li>
  <li><a href="#other-examples">Outros exemplos</a></li>
  <li><a href="#troubleshooting">Troubleshooting</a></li>
</ul>

<h2 id="começando">Começando</h2>

<p>Você pode começar a usar o GitLab Pages, com o Jekyll, facilmente bifurcando este repositório ou enviando (uploading) um novo/existente projeto do Jekyll.</p>

<p>Lembre-se de que você precisa esperar a construção do seu site antes de poder ver as alterações. Você pode rastrear a construção na guia <strong>Pipelines</strong>.</p>

<h3 id="comece-bifurcando-este-repositório">Comece bifurcando este repositório</h3>

<ol>
  <li>Bifurque (Fork) este repositório.</li>
  <li><strong>IMPORTANTE:</strong> Remova a relação do fork.
Vá para <strong>Settings (⚙)</strong> &gt; <strong>Edit Project</strong> e clique no botão <strong>“Remove fork relationship”</strong>.</li>
  <li>Habilite <em>Shared Runners</em>.
Vá para <strong>Settings (⚙)</strong> &gt; <strong>Pipelines</strong> e clique no botão <strong>“Enable shared Runners”</strong>.</li>
  <li>Renomeie o repositório para corresponder ao nome que você deseja para o seu site.</li>
  <li>Edite seu site por meio do GitLab ou clone o repositório e envie suas alterações.</li>
</ol>

<h3 id="comece-com-um-projeto-localmente-com-jekyll">Comece com um projeto localmente com Jekyll</h3>

<ol>
  <li><a href="https://jekyllrb.com/docs/installation/">Instale</a> o Jekyll.</li>
  <li>Use o comando <code class="language-plaintext highlighter-rouge">jekyll new</code> para criar um novo Projeto Jekyll.</li>
  <li>Este <a href=".gitlab-ci.yml">arquivo <code class="language-plaintext highlighter-rouge">.gitlab-ci.yml</code></a> à raiz do seu projeto.</li>
  <li>Envie seu repositório e alterações para o GitLab.</li>
</ol>

<h2 id="gitlab-ci">GitLab CI</h2>

<p>As páginas estáticas deste projeto são construídas pelo <a href="https://about.gitlab.com/gitlab-ci/">GitLab CI</a>, seguindo as etapas definidas em <a href=".gitlab-ci.yml"><code class="language-plaintext highlighter-rouge">.gitlab-ci.yml</code></a>:</p>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>image: ruby:latest

variables:
  JEKYLL_ENV: production

pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
</code></pre></div></div>

<h2 id="usando-jekyll-localmente">Usando Jekyll localmente</h2>

<p>Para trabalhar localmente com este projeto, você terá que seguir as etapas abaixo:</p>

<ol>
  <li>Faça um fork, clone ou baixe este projeto</li>
  <li><a href="https://jekyllrb.com/docs/installation/">Instale</a> o Jekyll</li>
  <li>Download da dependência: <code class="language-plaintext highlighter-rouge">bundle</code></li>
  <li>Construir e visualizar: <code class="language-plaintext highlighter-rouge">bundle exec jekyll serve</code></li>
  <li>Adicionar conteúdo</li>
</ol>

<p>Os comandos acima devem ser executados a partir do diretório raiz deste projeto.</p>

<p>Leia mais na <a href="https://jekyllrb.com/docs/home/">Documentação</a> do Jekyll.</p>

<h2 id="páginas-de-usuário-ou-grupo-do-gitlab">Páginas de usuário ou grupo do GitLab</h2>

<p>Para usar este projeto como seu site de usuário/grupo, você precisará de uma etapa adicional: basta renomear seu projeto para <code class="language-plaintext highlighter-rouge">namespace.gitlab.io</code>, onde <code class="language-plaintext highlighter-rouge">namespace</code> é o seu <code class="language-plaintext highlighter-rouge">username</code> ou <code class="language-plaintext highlighter-rouge">groupname</code>. Isso pode ser feito navegando até <strong>Settings</strong> do seu projeto.</p>

<p>Leia mais sobre <a href="https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages">user/group Pages</a> e <a href="https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages">project Pages</a>.</p>

<h2 id="você-bifurcou-este-projeto">Você bifurcou este projeto?</h2>

<p>Se você bifurcou este projeto para seu próprio uso, vá para <strong>Settings</strong> do seu projeto e remova a relação de bifurcação, que não será necessária, a menos que você queira contribuir de volta com o projeto.</p>

<h2 id="outros-exemplos">Outros exemplos</h2>

<ul>
  <li><a href="https://gitlab.com/pages/jekyll-branched">jekyll-branched</a> demonstra como você pode manter o seu site GitLab Pages em um branch e o código-fonte do seu projeto em outro.</li>
  <li>O grupo <a href="https://gitlab.com/jekyll-themes">jekyll-themes</a> contém uma coleção de projetos de exemplo que você pode bifurcar (como este) com diferentes estilos visuais.</li>
</ul>

<h2 id="troubleshooting">Troubleshooting</h2>

<ol>
  <li>CSS está faltando! Duas possibilidades:
    <ul>
      <li>Configuração incorreta da URL para o CSS em seus modelos</li>
      <li>gerador de site estático com opção de configuração que precisa ser definida explicitamente para servir ativos estáticos em uma URL relativa.</li>
    </ul>
  </li>
</ol>

:ET