---
layout: page
title: Sobre
sidebar_link: true
---

<p class="message">
  Meu nome é David Thimotti e moro em Belo Horizonte, Brasil. Atualmente, trabalho como Network Security Analyst. Minha formação é tecnólogo em Redes de Computadores, com  especialização em telecomunicações e redes de computadores pelo Centro Universitário Estácio de Belo Horizonte.
  <br><br>
  Estou interessado em engenharia de software em geral: estruturas de dados, algoritmos, protocolos, arquitetura, padrões de design e muito mais. Além disso, trabalho com redes de computadores corporativas e Cibersegurança. Recentemente, comecei a me envolver mais na comunidade de código aberto. E no meu tempo livre, gosto de jogar games, ler livros e assistir a filmes e séries.
  <br><br>
  Então, este é o meu blog (tentativa #2), um projeto pessoal, onde pretendo documentar meus estudos em programação e tecnologia em geral. Eu desejo que você possa aprender alguma coisa aqui. Se você quiser ficar em contato, deixe um comentário ou entre em contato comigo pelo Twitter.
</p>

Obrigado por ler!
